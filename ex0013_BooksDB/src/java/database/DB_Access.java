package database;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import pojo.Author;
import pojo.Book;

public class DB_Access {

    private static DB_Access instance;

    public static DB_Access getInstance() {
        if (instance == null) {
            instance = new DB_Access();
        }
        return instance;
    }

    private EntityManagerFactory emf;
    private EntityManager em;

    private DB_Access() {
        emf = Persistence.createEntityManagerFactory("Konrad_PLF3_2PU");
        em = emf.createEntityManager();
    }

    public List<Author> getListOfAuthors(String name) {
        TypedQuery<Author> tq
                = em.createNamedQuery("Author.findByLastname", Author.class);
        tq.setParameter("lastname", name);
        List<Author> authorList = tq.getResultList();
        return authorList;
    }

    public Book getBookOfAuthor(Author author) {
        return null;
    }

    public void close() {
        System.out.println("DB_Access : close !!!");
        em.close();
        emf.close();
    }
}
