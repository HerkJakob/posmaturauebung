/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author johann
 */
@Entity
@Table(name = "bookauthor")
@XmlRootElement
@NamedQueries(
        {
            @NamedQuery(name = "Bookauthor.findAll", query = "SELECT b FROM Bookauthor b")
            , @NamedQuery(name = "Bookauthor.findByAuthorId", query = "SELECT b FROM Bookauthor b WHERE b.bookauthorPK.authorId = :authorId")
            , @NamedQuery(name = "Bookauthor.findByRank", query = "SELECT b FROM Bookauthor b WHERE b.rank = :rank")
            , @NamedQuery(name = "Bookauthor.findByBookId", query = "SELECT b FROM Bookauthor b WHERE b.bookauthorPK.bookId = :bookId")
        })
public class Bookauthor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BookauthorPK bookauthorPK;
    @Column(name = "rank")
    private Short rank;
    @JoinColumn(name = "author_id", referencedColumnName = "author_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Author author;
    @JoinColumn(name = "book_id", referencedColumnName = "book_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Book book;

    public Bookauthor() {
    }

    public Bookauthor(BookauthorPK bookauthorPK) {
        this.bookauthorPK = bookauthorPK;
    }

    public Bookauthor(int authorId, int bookId) {
        this.bookauthorPK = new BookauthorPK(authorId, bookId);
    }

    public BookauthorPK getBookauthorPK() {
        return bookauthorPK;
    }

    public void setBookauthorPK(BookauthorPK bookauthorPK) {
        this.bookauthorPK = bookauthorPK;
    }

    public Short getRank() {
        return rank;
    }

    public void setRank(Short rank) {
        this.rank = rank;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookauthorPK != null ? bookauthorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookauthor)) {
            return false;
        }
        Bookauthor other = (Bookauthor) object;
        if ((this.bookauthorPK == null && other.bookauthorPK != null) || (this.bookauthorPK != null && !this.bookauthorPK.equals(other.bookauthorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pojo.Bookauthor[ bookauthorPK=" + bookauthorPK + " ]";
    }

}
