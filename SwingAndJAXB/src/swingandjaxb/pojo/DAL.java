package swingandjaxb.pojo;

import java.io.File;
import java.util.ArrayList;
import javax.xml.bind.JAXB;
import swingandjaxb.Inventory;

public class DAL {

    public void save(File file, Inventory inventory) {
        JAXB.marshal(inventory, file);
    }

    public ArrayList<? extends Product> load(File file) {
        Inventory unmarshalled = JAXB.unmarshal(file, Inventory.class);
        return unmarshalled.getProducts();
    }
}
