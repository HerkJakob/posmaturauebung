package swingandjaxb.pojo;

import java.time.LocalDate;
import java.util.ArrayList;

public class DVD extends Product{
    private ArrayList<Artist> roles;

    public DVD(ArrayList<Artist> roles, String title, LocalDate releaseDate, double price) {
        super(title, releaseDate, price);
        this.roles = roles;
    }

    public ArrayList<Artist> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<Artist> roles) {
        this.roles = roles;
    }
    
}
