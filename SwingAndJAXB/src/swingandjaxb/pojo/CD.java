package swingandjaxb.pojo;

import java.time.LocalDate;
import java.util.ArrayList;

public class CD extends Product {

    private ArrayList<String> tracks;
    private int length_in_seconds;

    public CD(ArrayList<String> tracks, int length_in_seconds, String title, LocalDate releaseDate, double price) {
        super(title, releaseDate, price);
        this.tracks = tracks;
        this.length_in_seconds = length_in_seconds;
    }

    public ArrayList<String> getTracks() {
        return tracks;
    }

    public void setTracks(ArrayList<String> tracks) {
        this.tracks = tracks;
    }

    public int getLength_in_seconds() {
        return length_in_seconds;
    }

    public void setLength_in_seconds(int length_in_seconds) {
        this.length_in_seconds = length_in_seconds;
    }
}
