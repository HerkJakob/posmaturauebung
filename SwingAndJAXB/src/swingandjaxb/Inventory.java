package swingandjaxb;

import java.io.File;
import swingandjaxb.pojo.Product;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import swingandjaxb.pojo.DAL;

@XmlRootElement(namespace = "testNamespace")
@XmlType(propOrder = {
    "productSize", "products"
})
public class Inventory extends AbstractTableModel {

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private ArrayList<Product> products = new ArrayList<>();
    private ArrayList<Product> old;

    @XmlElement(name = "total_size") //append to tag
    public int getProductSize() {
        return products.size();
    }

    private String[] titles = {
        "Title", "Release Date", "Price [€]"
    };

    @Override
    public int getRowCount() {
        return products.size();
    }

    @Override
    public int getColumnCount() {
        return titles.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = products.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return product.getTitle();
            case 1:
                LocalDate releaseDate = product.getReleaseDate();
                return DateTimeFormatter.ofPattern("dd. MMMM YYYY").format(releaseDate);
            default:
                return product.getPrice();
        }
    }

    @Override
    public String getColumnName(int column) {
        return titles[column];
    }

    public void addProduct(Product p) {
        products.add(p);
        this.fireTableRowsInserted(products.size() - 1, products.size() - 1);
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProductsFound(ArrayList<Product> found) {
        if (found == null) {
            return;
        }
        old = products;
        this.products = found;
        this.fireTableDataChanged();
    }

    public void clearSearch() {
        this.products = old;
        this.fireTableDataChanged();
    }

    public void save(File file) {
        DAL dal = new DAL();
        dal.save(file, this);
    }

    public void load(File file) {
        DAL dal = new DAL();
        ArrayList<? extends Product> load = dal.load(file);
        products.addAll(load);
        this.fireTableRowsInserted(products.size() - load.size(), products.size());
    }

}
