package swingandjaxb;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.SwingWorker;
import swingandjaxb.pojo.Artist;
import swingandjaxb.pojo.DVD;
import swingandjaxb.pojo.Product;

public class SearchDVDSwingWorker extends SwingWorker<ArrayList<Product>, String> {

    private ArrayList<Product> allProducts;

    private DVD searchDVD;

    public SearchDVDSwingWorker(ArrayList<Product> allProducts, DVD searchDVD) {
        this.allProducts = allProducts;
        this.searchDVD = searchDVD;
    }

    @Override
    protected ArrayList<Product> doInBackground() throws Exception {
        Stream<Product> allDvdsStream = allProducts.stream().filter(p -> p instanceof DVD);

        if (searchDVD.getTitle() != null) {
            allDvdsStream = allDvdsStream.filter(p -> p.getTitle().equalsIgnoreCase(searchDVD.getTitle()));
        }

        if (searchDVD.getReleaseDate() != null) {
            allDvdsStream = allDvdsStream.filter(p -> p.getReleaseDate().equals(searchDVD.getReleaseDate()));
        }
        for (int i = 0; i < 10; i++) {
            Thread.sleep(10);
            System.out.println("swingworker is sleeping");
        }

        if (searchDVD.getPrice() > 0) {
            allDvdsStream = allDvdsStream.filter(p -> p.getPrice() == searchDVD.getPrice());
        }

        ArrayList<Artist> roles = searchDVD.getRoles();
        if (roles != null && !roles.isEmpty()) {
            allDvdsStream = allDvdsStream.filter((Product p) -> {
                return ((DVD) p).getRoles().containsAll(roles);
            });
        }

        return (ArrayList) allDvdsStream.collect(Collectors.toList());
    }

}
