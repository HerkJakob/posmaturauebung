package swingandjaxb.Dialogs;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class MyCanvas extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        try {
            super.paintComponent(g);
            File f = new File(".../Assets/toDraw.png"); //TODO Figure out where to locate img so that it can be accessed when launched form net beans and from terminal
            System.out.println(f.getAbsolutePath());
            BufferedImage img = ImageIO.read(f);
            g.drawImage(img, 0, 0, getWidth(), getHeight(), null);
        } catch (IOException ex) {
            Logger.getLogger(MyCanvas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
