package database;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import pojo.Auto;
import pojo.Marke;
import pojo.Standort;

public class DB_Access {

    private EntityManagerFactory emf;
    private EntityManager em;

    private DB_Access() {
        emf = Persistence.createEntityManagerFactory("AutohaendlerPU");
        em = emf.createEntityManager();
    }

    public List<Standort> getStandorte() {
        Query query = em.createNamedQuery("Standort.getStandorte");
        List<Standort> resultList = query.getResultList();
        if (resultList == null) {
            return null;
        }
        return resultList.stream().sorted((Standort o1, Standort o2) -> o1.getName().compareTo(o2.getName())).collect(Collectors.toList());
    }

    public List<Marke> getMarken() {
        Query query = em.createNamedQuery("Marke.getMarken");
        List<Marke> resultList = query.getResultList();
        if (resultList == null) {
            return null;
        }
        return resultList.stream().sorted((Marke m1, Marke m2) -> m1.getName().compareTo(m2.getName())).collect(Collectors.toList());
    }

    public List<Auto> getAutos(String standort, String marke) {
        Query query = em.createNamedQuery("Auto.getAutosByStandortAndMarke");
        query.setParameter("standort", standort);
        if (marke == null) {
            query.setParameter("marke", "%");
        } else {
            query.setParameter("marke", marke);
        }

        List<Auto> resultList = query.getResultList();
        if (resultList == null) {
            return resultList;
        }
        return resultList.stream().sorted((Auto a1, Auto a2) -> (int) (a2.getPreis() * 100 - a1.getPreis() * 100)).collect(Collectors.toList());
    }

    public static DB_Access getInstance() {
        return DB_AccessHolder.INSTANCE;
    }

    private static class DB_AccessHolder {
        private static final DB_Access INSTANCE = new DB_Access();
    }
}
