package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity()
@NamedQueries(value = {
    @NamedQuery(name = "Marke.getMarken", query = "SELECT m FROM Marke m")
})
public class Marke implements Serializable {

    @Id
    private long id;
    private String name;

    @OneToMany(mappedBy = "marke")
    private List<Auto> autos = new ArrayList<>();

    public Marke() {
    }

    public Marke(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Auto> getAutos() {
        return autos;
    }

    public boolean addAuto(Auto a) {
        a.setMarke(this);
        return autos.add(a);
    }

}
