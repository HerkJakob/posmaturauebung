package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries(value = {
    @NamedQuery(name = "Standort.getStandorte", query = "SELECT s FROM Standort s")
})
public class Standort implements Serializable {

    @Id
    private long id;
    private String name;

    @OneToMany(mappedBy = "standort")
    private List<Auto> autos = new ArrayList<>();

    public Standort() {
    }

    public Standort(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<Auto> getAutos() {
        return autos;
    }
    
    public boolean addAuto(Auto a){
        a.setStandort(this);
        return autos.add(a);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
