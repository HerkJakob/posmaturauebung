package pojo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(value = {
    @NamedQuery(name = "Auto.getAutosByStandortAndMarke", query = "SELECT a FROM Auto a WHERE a.marke.name LIKE :marke AND a.standort.name = :standort")
})
public class Auto implements Serializable {

    @ManyToOne
    private Standort standort;
    @ManyToOne
    private Marke marke;

    public Auto() {
    }

    public Standort getStandort() {
        return standort;
    }

    public void setStandort(Standort standort) {
        this.standort = standort;
    }

    public Marke getMarke() {
        return marke;
    }

    public void setMarke(Marke marke) {
        this.marke = marke;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public double getLeistung() {
        return leistung;
    }

    public void setLeistung(double leistung) {
        this.leistung = leistung;
    }

    public Auto(Standort standort, Marke marke, long id, String name, double preis, double leistung) {
        this.standort = standort;
        this.marke = marke;
        this.id = id;
        this.name = name;
        this.preis = preis;
        this.leistung = leistung;
    }
    @Id
    private long id;
    private String name;
    private double preis;
    private double leistung;

}
