package hibernatetest;

import ComposedPK.Kunde_embedded;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import basics.Kunde1_basics;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import relationships.bi_one_to_one.Adresse_RS_B;
import relationships.bi_one_to_one.Kunde_RS_B;
import relationships.n_to_m.Mitarbeiter;
import relationships.n_to_m.PKW;
import relationships.one_to_n.Buchung;
import relationships.one_to_n.Konto;
import relationships.uni_one_to_one.Adresse_RS;
import relationships.uni_one_to_one.Kunde_RS;

public class HibernateTest {

    private static EntityManagerFactory emf;
    private static EntityManager em;

    public static void main(String[] args) {
        try {
            //verbinde zu DB
            emf = Persistence.createEntityManagerFactory("HibernateTestPU");
            em = emf.createEntityManager();

            //wichtig, da sonst nichts geschrieben wird
            em.getTransaction().begin();

            bsp1();
            em.getTransaction().commit();
            em.getTransaction().begin();
            bsp2();
            em.getTransaction().commit();
            em.getTransaction().begin();
            bsp3();
            bsp4();
            bsp5();
            bsp6();
            bsp7();
            bsp8();
            bsp9();
            bsp10();
            bsp11();
            bsp12();

            //schreiben
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //egal ob Fehler da oder nicht: Schließt Verbindung
            //em vor emf schließen --> ist wichtig
            if (em != null) {
                em.close();
            }
            if (emf != null) {
                emf.close();
            }
        }
    }

    /**
     * einfaches schreiben
     */
    private static void bsp1() {
        Kunde1_basics k1 = new Kunde1_basics("vorname", "nachname");
        em.persist(k1);
    }

    /**
     * Kunde mit IdClass
     */
    private static void bsp2() {
        ComposedPK.Kunde_embedded kunde = new Kunde_embedded("abt", 1);
        em.persist(kunde);
    }

    /**
     * finde per ID
     */
    private static void bsp3() {
        //Can't find composed PK
        //ComposedPK.Kunde1 found = em.find(ComposedPK.Kunde1.class, new ComposedPK.KundeID1("abt", 1));
        //richtig:
        List resultList = em.createQuery("SELECT k FROM Kunde1 k WHERE k.id.num = :id").setParameter("id", 1).getResultList();
        if (resultList.size() > 0) {
            System.out.println(resultList.get(0));
        } else {
            System.out.println("nothing found 3");
        }
    }

    /**
     * update a value
     */
    private static void bsp4() {
        List resultList = em.createQuery("SELECT k FROM Kunde1 k WHERE k.id.num = :id").setParameter("id", 1).getResultList();
        if (resultList.size() > 0) {
            Kunde_embedded found = (Kunde_embedded) resultList.get(0);
            found.setAge(1000);
        } else {
            System.out.println("nothing found 4");
        }
        bsp3();
    }

    /**
     * löschen
     */
    private static void bsp5() {
        List resultList = em.createQuery("SELECT k FROM Kunde1 k WHERE k.id.num = :id").setParameter("id", 1).getResultList();
        resultList.forEach(x -> em.remove(x));
        bsp3();
    }

    /**
     * 1:1 unidirektional
     */
    private static void bsp6() {
        Adresse_RS adresse = new Adresse_RS("ort", "str", "plz", "AUT");
        Kunde_RS kunde = new Kunde_RS(1, adresse, "herk");
//        em.persist(adresse);
        em.persist(kunde);
        em.getTransaction().commit();
        em.getTransaction().begin();

        //adresse wird automatisch mitgeladen
        Kunde_RS find = em.find(Kunde_RS.class, 1);
        System.out.println(find);
    }

    /**
     * 1:1 bidirektional
     */
    private static void bsp7() {
        relationships.bi_one_to_one.Kunde_RS_B kunde = new Kunde_RS_B(0, null, "herk");
        relationships.bi_one_to_one.Adresse_RS_B adresse_RS_B = new Adresse_RS_B(1, "ort", "strasse", "plz", "AUT", kunde);
        kunde.setAdresse(adresse_RS_B);

        em.persist(kunde);
    }

    /**
     * finde bidirektionale 1:1
     */
    private static void bsp8() {
        Adresse_RS_B found = em.find(Adresse_RS_B.class, 1);
        System.out.println(found.getKunde().toString());
    }

    private static void bsp9() {
        Konto konto = new Konto(1);
        for (int i = 0; i < 10; i++) {
            konto.addBuchung(new Buchung(i, BigDecimal.ZERO, new Date(), "test"));
        }

        em.persist(konto);
    }

    /**
     * finde Konto mit Buchungen
     */
    private static void bsp10() {
        Konto konto = em.find(Konto.class, 1);
        System.out.println(konto);
    }

    /**
     * füge n:m hinzu
     */
    private static void bsp11() {
        List<Mitarbeiter> mitarbeiter = new ArrayList<>();
        List<PKW> pkws = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            PKW pkw = new PKW(i, "mclaren", "p1", 916);
            pkws.add(pkw);
        }

        
        for (int i = 0; i < 10; i++) {
            Mitarbeiter m = new Mitarbeiter(i, "M" + i);
            mitarbeiter.add(m);
            Collections.shuffle(pkws);
            for (int j = 0; j < 4; j++) {
                m.addPKW(pkws.get(j));
            }
        }
        mitarbeiter.forEach(x -> em.persist(x));
    }
    
    /**
     * finde n:m
     */
    private static void bsp12(){
        Mitarbeiter mitarbeiter = em.find(Mitarbeiter.class, 1);
        System.out.println(mitarbeiter);
    }
}
