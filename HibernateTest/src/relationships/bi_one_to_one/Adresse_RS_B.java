package relationships.bi_one_to_one;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Adresse_RS_B implements Serializable {

    @Id
    private int id;
    private String ort, strasse, plz, land;
    
    //ohne mappedBy würde Hibernate denken, es wären zwei von einander unabhängige Beziehungen
    @OneToOne(mappedBy = "adresse")
    private Kunde_RS_B kunde;

    public Adresse_RS_B() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kunde_RS_B getKunde() {
        return kunde;
    }

    public void setKunde(Kunde_RS_B kunde) {
        this.kunde = kunde;
    }

    public Adresse_RS_B(int id,String ort, String strasse, String plz, String land, Kunde_RS_B kunde) {
        this.ort = ort;
        this.id = id;
        this.strasse = strasse;
        this.plz = plz;
        this.kunde = kunde;
        this.land = land;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }
}
