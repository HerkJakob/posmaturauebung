package relationships.bi_one_to_one;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Kunde_RS_B implements Serializable {
//cascade: speichert attribut automatisch, wenn objekt gespeichert wird
    //orphanRemoval löscht adresse, wenn dieses objekt gelöscht wird

    @OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
    //@JoinColumn setzt Name für PK
    @JoinColumn(name = "adresse")
    private Adresse_RS_B adresse;

    private String name;
    @Id
    private int id;

    @Override
    public String toString() {
        return "Kunde_RS_B{" + "adresse=" + adresse + ", name=" + name + ", id=" + id + '}';
    }

    public Kunde_RS_B() {

    }

    public Kunde_RS_B(int id, Adresse_RS_B adresse, String name) {
        this.adresse = adresse;
        this.name = name;
        this.id = id;
    }

    public Adresse_RS_B getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse_RS_B adresse) {
        this.adresse = adresse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
