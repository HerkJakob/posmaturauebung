package relationships.uni_one_to_one;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Adresse_RS implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private String ort, strasse, plz, land;

    public Adresse_RS() {

    }

    public Adresse_RS(String ort, String strasse, String plz, String land) {
        this.ort = ort;
        this.strasse = strasse;
        this.plz = plz;
        this.land = land;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

}
