package relationships.n_to_m;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;

@Entity
public class Mitarbeiter implements Serializable {

    @Id
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PKW> getPkws() {
        return pkws;
    }

    public void setPkws(List<PKW> pkws) {
        this.pkws = pkws;
    }

    public Mitarbeiter(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @ManyToMany(mappedBy = "mitarbeiter", cascade = CascadeType.PERSIST)
    @OrderBy(value = "ps ASC, id DESC") //wird nach ps geordnet
    private List<PKW> pkws = new ArrayList<>();
    
    public void addPKW(PKW pkw){
        this.pkws.add(pkw);
        pkw.addMitarbeiter(this);
    }
    
    public Mitarbeiter(){
        
    }

    @Override
    public String toString() {
        return "Mitarbeiter{" + "id=" + id + ", name=" + name + ", pkws=" + pkws.size() + '}';
    }
}
