package relationships.one_to_n;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Konto implements Serializable {

    @Override
    public String toString() {
        return "Konto{" + "id=" + id + ", kontostand=" + kontostand + ", buchungen=" + buchungen.size() + '}';
    }
    
    @Id
    private int id;
    @Column(scale = 2, precision = 10)
    private BigDecimal kontostand = BigDecimal.ZERO;
    
    @OneToMany(mappedBy = "konto", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private List<Buchung> buchungen = new ArrayList<>();
    
    public Konto(int id){
        this.id = id;
    }
    
    public void addBuchung(Buchung buchung){
        buchung.setKonto(this);
        this.buchungen.add(buchung);
        kontostand.add(buchung.getBetrag());
    }
    
    public Konto(){
        
    }
}
