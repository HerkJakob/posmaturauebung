package relationships.one_to_n;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Buchung implements Serializable {

    @Id
    private int id;
    @ManyToOne
    private Konto konto;

    @Column(scale = 2, precision = 10)
    private BigDecimal betrag;
    @Temporal(TemporalType.DATE)
    private Date datum;
    private String text;

    public Buchung(int id, BigDecimal betrag, Date datum, String text) {
        this.id = id;
        this.betrag = betrag;
        this.datum = datum;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Konto getKonto() {
        return konto;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }

    public BigDecimal getBetrag() {
        return betrag;
    }

    public void setBetrag(BigDecimal betrag) {
        this.betrag = betrag;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Buchung() {

    }
}
