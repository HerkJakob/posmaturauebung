/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vererbung;

/**
 *
 * @author jakob
 */
public class MeineSingleton {
    
    private MeineSingleton() {
    }
    
    public static MeineSingleton getInstance() {
        return MeineSingletonHolder.INSTANCE;
    }
    
    private static class MeineSingletonHolder {
        private static final MeineSingleton INSTANCE = new MeineSingleton();
    }
}
