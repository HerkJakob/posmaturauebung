package ComposedPK;

import java.io.Serializable;
import java.util.Objects;

public class KundeID implements Serializable {

    private int num;
    private String abt;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.num;
        hash = 47 * hash + Objects.hashCode(this.abt);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KundeID other = (KundeID) obj;
        if (this.num != other.num) {
            return false;
        }
        if (!Objects.equals(this.abt, other.abt)) {
            return false;
        }
        return true;
    }

}
