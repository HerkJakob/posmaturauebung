package ComposedPK;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(KundeID.class)
@Table(name = "Kunde_cpk")
public class Kunde implements Serializable {
    @Id
    @Column(length = 100)//muss hier gesetzt werden
    private String abt;
    
    @Id
    private int num;
    
    public Kunde(){
        
    }

    public Kunde(String abt, int i) {
        this.abt = abt;
        this.num = i;
    }
}
