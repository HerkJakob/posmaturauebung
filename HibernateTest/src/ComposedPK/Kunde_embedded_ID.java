package ComposedPK;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public class Kunde_embedded_ID implements Serializable {
    private String abt;
    private int num;

    public Kunde_embedded_ID(String abt, int i) {
        this.abt = abt;
        this.num = i;
    }
    
    public Kunde_embedded_ID(){
        
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.abt);
        hash = 17 * hash + this.num;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Kunde_embedded_ID other = (Kunde_embedded_ID) obj;
        if (this.num != other.num) {
            return false;
        }
        if (!Objects.equals(this.abt, other.abt)) {
            return false;
        }
        return true;
    }
    
    
}
