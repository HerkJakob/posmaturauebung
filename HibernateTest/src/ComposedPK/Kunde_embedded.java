package ComposedPK;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Kunde1_cpk")
public class Kunde_embedded implements Serializable {

    @EmbeddedId
    private Kunde_embedded_ID id;

    private int age = 10;

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Kunde1{" + "id=" + id + ", age=" + age + '}';
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Kunde_embedded(String abt, int i) {
        id = new Kunde_embedded_ID(abt, i);
    }

    public Kunde_embedded() {

    }
}
