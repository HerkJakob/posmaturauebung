package basics;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

public class Kunde2 {
    @Id
    /**
     * 4 methoden für Generieren von PK
     * 1) AUTO --> Default value, JPA nimmt dann selbst IDENTITY oder SEQUENCE
     * 2) IDENTITY --> generiert id, die immer um eins höher ist als letzte 
     * 3) TABLE --> erzeugt neue Tabelle für PK, unnötig
     * 4) SEQUENCE --> erzeugt Sequence auf DB
     *                 mit @SequenceGenerator kann man Properties setzen
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    //@Column speichert Eigenschaft, die ein Attribut in Tabelle haben soll
    //mit @columnDefinition kann man direkt DDL code übergeben
    @Column(length = 100, unique = false)
    private String vorname;
    //variablen dürfen nicht 'final' sein
    private String nachname;
    
    //wird nicht gespeichert
    @Transient
    private int alter;
    
    //scale: nachkommastellen, preciscion: stellen vor dem Komma
    //darf nur bei BigDecimal verwendet werden
    @Column(nullable = false, scale = 2, precision = 3)
    private BigDecimal money;
    
    @Temporal(TemporalType.DATE)
    private Date gebDatum;

    public Kunde2(){
        
    }
}
