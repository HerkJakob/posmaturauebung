package basics;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //damit Hibernate weiß, dass das ein Table ist
@Table(name = "Kunde_table_test") //den Namen des Tables setzen
public class Kunde1_basics implements Serializable {

    @Id
    private String id;
    private String vorname;
    private String nachname;

    public Kunde1_basics(String id, String vorname, String nachname) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
    }

    //Default konstruktor --> muss in Hibernate immer vorhanden sein
    public Kunde1_basics() {
        this.id = UUID.randomUUID().toString();
    }

    public Kunde1_basics(String vorname, String nachname) {
        //rufe default konstruktor auf
        this();
        //initialisiere
        this.vorname = vorname; 
        this.nachname = nachname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }
}
