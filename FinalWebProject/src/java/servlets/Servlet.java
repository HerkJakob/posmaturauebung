package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.print("doGet in Servlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String number = request.getParameter("number");
        try (PrintWriter pw = response.getWriter()) {
            request.setAttribute("username", username);
            request.setAttribute("number", number);
            String words[] = {"eins", "zwei", "drei"};
            request.setAttribute("words", words);
            request.getRequestDispatcher("DisplayAllElements.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
